const express = require("express");
const fetch = require("node-fetch");
const film_db = require("better-sqlite3")('dh_user_db.db3', {});
const user_db = require("better-sqlite3")('dh_userdata.db3', {});
const { MongoClient, ServerApiVersion } = require("mongodb");
const genUsername = require("unique-username-generator");
const bcrypt = require("bcrypt");
const http = require("http");
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies


app.post("/user/register", async (req, res) => {
    try {
        // generate a username
        let username = await genUsername.generateUsername("-", 3);
        // generate a salt
        let salt = await bcrypt.genSalt(10);
        // hash the password
        let hash = await bcrypt.hash(req.body.password, salt);
        // insert the user into the database
        // generate a random API key
        let api_key = await Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        let user = await user_db.prepare(`INSERT INTO Users (username, password, email, salt, api_key) VALUES (?, ?, ?, ?, ?)`)
            .run(username, hash, req.body.email, salt, api_key);
        // return the user their username
        res.json({
            status: 200,
            username: username,
            api_key: api_key
        });
    } catch (e) {
        console.log(e);
        res.json({
            status: 500,
            error: e
        });
    }
});

app.post("/user/update", async (req, res) => {
    try {
        // check the bearer token in header, if it matches the user's api key, update the user's password
        let user = await user_db.prepare(`SELECT * FROM Users WHERE api_key = ?`).get(req.headers.authorization.split(" ")[1]);
        if (user) {
            // add the age and gender of the uzer
            await user_db.prepare(`UPDATE Users SET age = ?, gender = ?, name = ?`).run(req.body.age, req.body.gender, req.body.name);
            res.json({
                status: 200
            })
        } else {
            res.json({
                status: 401,
                error: "Unauthorized"
            });
        }
    } catch (e) {
        console.log(e);
        res.json({
            status: 500,
            error: e
        });
    }
});

app.get("/user/list_init", async (req, res) => {
    try {
        let film_ids = [238, 129, 496243, 155, 497, 324857, 1891, 157336, 122, 299534, 603, 218, 77338, 10681, 289, 348, 138843, 694, 52449, 6620, 2105, 262551, 862, 1588, 1584, 11631, 181808];
        let films = [];
        // for each film get basic details from film_db
        for (let i = 0; i < film_ids.length; i++) {
            let film = await film_db.prepare(`SELECT * FROM Film WHERE id = ?`).get(film_ids[i]);
            films.push(film);
        }
        // also add 13 random movies from db
        let random_ids = [];
        for (let i = 0; i < 13; i++) {
            let random_id = await Math.floor(Math.random() * 20000);
            while (random_ids.includes(random_id)) {
                random_id = await Math.floor(Math.random() * 20000);
            }
            random_ids.push(random_id);
        }
        for (let i = 0; i < random_ids.length; i++) {
            let film = await film_db.prepare(`SELECT * FROM Film WHERE id = ?`).get(random_ids[i]);
            if (film == null) continue;
            films.push(film);
        }
        // sort films by random
        films.sort(() => Math.random() - 0.5);
        res.json({
            status: 200,
            films: films
        });
    } catch (e) {
        console.log(e);
        res.json({
            status: 500,
            error: e
        });
    }
});

app.post("/user/list_init", async (req, res) => {
    try {
        // check auth
        let user = await user_db.prepare(`SELECT * FROM Users WHExRE api_key = ?`).get(req.headers.authorization.split(" ")[1]);
        if (user) {
            let films = req.body.liked_films;
            // add the films to the user's liked films
            for (let i = 0; i < films.length; i++) {
                // for each film create a liked film entry
                await user_db.prepare(`INSERT INTO Liked (user_id, film_id) VALUES (?, ?)`).run(user.id, films[i]);
                // fetch the reccomended films
                let rec = await fetch(`https://api.themoviedb.org/3/movie/${films[i]}/recommendations?api_key=e089a0b66a4b4a063793c50fecfb0c7d&language=en-US&page=1`).then(res => res.json());
                // for each rec.results, create a recommended film entry
                for (let j = 0; j < rec.results.length; j++) {
                    // check if its already in the db in the liked or reccomended table
                    let liked = await user_db.prepare(`SELECT * FROM Liked WHERE user_id = ? AND film_id = ?`).get(user.id, rec.results[j].id);
                    let recomended = await user_db.prepare(`SELECT * FROM Recommended WHERE user_id = ? AND film_id = ?`).get(user.id, rec.results[j].id);
                    if (liked == null && recomended == null) {
                        await user_db.prepare(`INSERT INTO Recommended (user_id, film_id) VALUES (?, ?)`).run(user.id, rec.results[j].id);
                    }
                }
            }
        } else {
            res.json({
                status: 401,
                error: "Unauthorized"
            });
        }
    } catch (e) {
        console.log(e);
        res.json({
            status: 500,
            error: e
        });
    }
});

app.post("/user/login", async (req, res) => {
    try {
        let data = req.body;
        // try to match username and password
        let user = await user_db.prepare(`SELECT * FROM Users WHERE username = ? OR email = ?`).get(req.body.username, req.body.email);
        if (user) {
            // salt is stored in user, check if it matches the password
            let passc = await bcrypt.hash(req.body.password, user.salt);
            let pass = passc == user.password;
            if (pass) {
                // if it matches, return the user's api key
                res.json({
                    status: 200,
                    api_key: user.api_key
                });
            }
            else {
                res.json({
                    status: 401,
                    error: "Unauthorized"
                });
            }
        } else {
            res.json({
                status: 404,
                error: "User not found"
            });
        }
    } catch (e) {
        console.log(e);
        res.json({
            status: 500,
            error: e
        });
    }
});

app.get("/user/get_recccomendation_list", async (req, res) => {
    try {
        // check auth
        let user = await user_db.prepare(`SELECT * FROM Users WHERE api_key = ?`).get(req.headers.authorization.split(" ")[1]);
        if (user) {
            // select all reccomended films
            let films = await user_db.prepare(`SELECT * FROM Recommended WHERE user_id = ?`).all(user.id);
            // for each film get basic details from film_db
            for (let i = 0; i < films.length; i++) {
                let film = await film_db.prepare(`SELECT * FROM Film WHERE id = ?`).get(films[i].film_id);
                films[i] = film;
            }
            res.json({
                status: 200,
                films: films
            });
        } else {
            res.json({
                status: 401,
                error: "Unauthorized"
            });
        }
    } catch (e) {
        console.log(e);
        res.json({
            status: 500,
            error: e
        });
    }
});

app.get("/movie/details/:id", async (req, res) => {
    try {
        let data = await fetch(`https://api.themoviedb.org/3/movie/${req.params.id}?api_key=e089a0b66a4b4a063793c50fecfb0c7d&language=en-US`).then(res => res.json());
        data.video = await fetch(`https://api.themoviedb.org/3/movie/${req.params.id}/videos?api_key=e089a0b66a4b4a063793c50fecfb0c7d&language=en-US`).then(res => res.json());
        res.json({
            status: 200,
            data: data
        })
    } catch (e) {
        console.log(e);
        res.json({
            status: 500,
            error: e
        });
    }
});

app.get("/user/movie_opinion/:id", async (req, res) => {
    try {
        // check auth
        let user = await user_db.prepare(`SELECT * FROM Users WHERE api_key = ?`).get(req.headers.authorization.split(" ")[1]);
        let similars = [];
        if (user) {
            // if the query has liked or watched_like, add it to liked table
            if (req.query.liked || req.query.watched_like) {
                // get reccomendations for this movie
                let rec = await fetch(`https://api.themoviedb.org/3/movie/${req.params.id}/recommendations?api_key=e089a0b66a4b4a063793c50fecfb0c7d&language=en-US&page=1`).then(res => res.json());
                // for each rec.results, create a recommended film entry
                for (let j = 0; j < rec.results.length; j++) {
                    // check if its already in the db in the liked or reccomended table
                    let liked = await user_db.prepare(`SELECT * FROM Liked WHERE user_id = ? AND film_id = ?`).get(user.id, rec.results[j].id);
                    let recomended = await user_db.prepare(`SELECT * FROM Recommended WHERE user_id = ? AND film_id = ?`).get(user.id, rec.results[j].id);
                    let disliked = await user_db.prepare(`SELECT * FROM Disliked WHERE user_id = ? AND film_id = ?`).get(user.id, rec.results[j].id);
                    if (liked == null && recomended == null && disliked == null) {
                        await user_db.prepare(`INSERT INTO Recommended (user_id, film_id) VALUES (?, ?)`).run(user.id, rec.results[j].id);
                    }
                }
                // remove the reccomendation from the reccomended table
                await user_db.prepare(`DELETE FROM Recommended WHERE user_id = ? AND film_id = ?`).run(user.id, req.params.id);
                // check if another user from the friendlist liked this movie
                let friends = await user_db.prepare(`SELECT * FROM Friends WHERE user_id_1 = ? OR user_id_2 = ?`).all(user.id, user.id);
                for (let i = 0; i < friends.length; i++) {
                    let liked = await user_db.prepare(`SELECT * FROM Liked WHERE user_id = ? AND film_id = ?`).get(friends[i].user_id_1 == user.id ? friends[i].user_id_2 : friends[i].user_id_1, req.params.id);
                    if (liked) {
                        // push the friend to the similars array
                        similars.push(friends[i]);
                    }
                }
            } else {
                // remove the reccomendation and add it to the disliked table
                await user_db.prepare(`DELETE FROM Recommended WHERE user_id = ? AND film_id = ?`).run(user.id, req.params.id);
                await user_db.prepare(`INSERT INTO Disliked (user_id, film_id) VALUES (?, ?)`).run(user.id, req.params.id);
                // check if another user from the friendlist disliked this movie
                let friends = await user_db.prepare(`SELECT * FROM Friends WHERE user_id_1 = ? OR user_id_2 = ?`).all(user.id, user.id);
                for (let i = 0; i < friends.length; i++) {
                    let disliked = await user_db.prepare(`SELECT * FROM Disliked WHERE user_id = ? AND film_id = ?`).get(friends[i].user_id_1 == user.id ? friends[i].user_id_2 : friends[i].user_id_1, req.params.id);
                    if (disliked) {
                        // push the friend to the similars array
                        similars.push(friends[i]);
                    }
                }
            }
            res.json({
                status: 200,
                similars: similars
            });
        } else {
            res.json({
                status: 401,
                error: "Unauthorized"
            });
        }
    } catch (e) {
        console.log(e);
        res.json({
            status: 500,
            error: e
        });
    }
});

app.get("/user/movies/list", async (req, res) => {
    try {
        // check auth
        let user = await user_db.prepare(`SELECT * FROM Users WHERE api_key = ?`).get(req.headers.authorization.split(" ")[1]);
        if (user) {
            res.json({
                status: 200,
                liked: await user_db.prepare(`SELECT * FROM Liked WHERE user_id = ?`).all(user.id),
                disliked: await user_db.prepare(`SELECT * FROM Disliked WHERE user_id = ?`).all(user.id),
                recommended: await user_db.prepare(`SELECT * FROM Recommended WHERE user_id = ?`).all(user.id)
            });
        } else {
            res.json({
                status: 401,
                error: "Unauthorized"
            });
        }
    } catch (e) {
        console.log(e);
        res.json({
            status: 500,
            error: e
        });
    }
});

app.get("/user/info", async (req, res) => {
    try {
        // check auth
        let user = await user_db.prepare(`SELECT * FROM Users WHERE api_key = ?`).get(req.headers.authorization.split(" ")[1]);
        if (user) {
            // remove salt and password and api key
            delete user.salt;
            delete user.password;
            delete user.api_key;
            res.json({
                status: 200,
                user: user
            });
        } else {
            res.json({
                status: 401,
                error: "Unauthorized"
            });
        }
    } catch (e) {
        console.log(e);
        res.json({
            status: 500,
            error: e
        });
    }
});

app.get("/user/friends/list", async (req, res) => {
    try {
        // check auth
        let user = await user_db.prepare(`SELECT * FROM Users WHERE api_key = ?`).get(req.headers.authorization.split(" ")[1]);
        if (user) {
            // get all friends
            let friends = await user_db.prepare(`SELECT * FROM Friends WHERE user_id_1 = ? OR user_id_2 = ?`).all(user.id, user.id);
            // get username and name of each friend
            for (let i = 0; i < friends.length; i++) {
                let friend = await user_db.prepare(`SELECT * FROM Users WHERE id = ?`).get(friends[i].user_id_1 == user.id ? friends[i].user_id_2 : friends[i].user_id_1);
                friends[i].username = friend.username;
                friends[i].name = friend.name;
            }
            res.json({
                status: 200,
                friends: friends
            });
        } else {
            res.json({
                status: 401,
                error: "Unauthorized"
            });
        }
    } catch (e) {
        console.log(e);
        res.json({
            status: 500,
            error: e
        });
    }
});

app.post("/user/friends/add", async (req, res) => {
    try {
        // check auth
        let user = await user_db.prepare(`SELECT * FROM Users WHERE api_key = ?`).get(req.headers.authorization.split(" ")[1]);
        if (user) {
            // resolve username of other friend
            let friend = await user_db.prepare(`SELECT * FROM Users WHERE username = ?`).get(req.body.username);
            if (friend) {
                // check if the user is already a friend
                let already_friend = await user_db.prepare(`SELECT * FROM Friends WHERE (user_id_1 = ? AND user_id_2 = ?) OR (user_id_1 = ? AND user_id_2 = ?)`).get(user.id, friend.id, friend.id, user.id);
                if (!already_friend) {
                    // add friend
                    await user_db.prepare(`INSERT INTO Friends (user_id_1, user_id_2) VALUES (?, ?)`).run(user.id, friend.id);
                    res.json({
                        status: 200,
                        message: "Friend added"
                    });
                } else {
                    res.json({
                        status: 400,
                        error: "Already a friend"
                    });
                }
            } else {
                res.json({
                    status: 400,
                    error: "Friend not found"
                });
            }
        } else {
            res.json({
                status: 401,
                error: "Unauthorized"
            });
        }
    } catch (e) {
        console.log(e);
        res.json({
            status: 500,
            error: e
        });
    }
});

app.get("/user/sploshno", async (req, res) => {
    try {
        // check auth
        let user = await user_db.prepare(`SELECT * FROM Users WHERE api_key = ?`).get(req.headers.authorization.split(" ")[1]);
        if (user) {
        } else {
            res.json({
                status: 401,
                error: "Unauthorized"
            });
        }
    } catch (e) {
        console.log(e);
        res.json({
            status: 500,
            error: e
        });
    }
})


http.createServer(app).listen(3000);

