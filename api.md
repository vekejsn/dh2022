# Definirane metode

## /user/register
Tip metode: POST
```
req.params: (X-)
    email: sintaktično pravilen email
    password: string dolžine vsaj 6

response:
    status: 200, 404, 500...
    username: avtomatski generiran username uporabnika (če je status 200)
    api_key: Bearer (shraniti v cookie ali nekjer)
```
## /user/update
Tip metode: POST
Authorization: Bearer token
```
req_params:
    age: integer
    gender: char (M,F,O)
    movies: boolean NOT IMPLEMENTED
    shows: boolean NOT IMPLEMENTED

response:
    status: 200, 404, 500...
```

## /user/list_init
Tip metode: GET
Authorization: Bearer token
```
response:
    status: 200, 404, 500...
    init_list: [množica objektov filmov]

movie_object:
    {
        "id": integer,
        "title": string,
        "image": string [url na sliko]
    }
```
^ - ta klic morda ne bi bilo slabo predefinirati in vsem pošiljati iste začetne filme

## /user/login
Tip metode: POST
```
req.params:
    username: string,
    password: string

response:
    status: 200, 404, 500...
    api_key: string
```

## /user/get_recccomendation_list
Tip metode: GET
Authorization: Bearer token
```
response:
    status: 200, 404, 500...
    list: [množica objektov filmov kot za list init]
```

## /movies/details/:id
Tip metode: GET
Authorization: Bearer token
```
response:
    status: 200, 404, 500...
    data: [podatki sa TMDB API-ja + videji]
```

## /user/movie_opinion/:id
Tip metode: POST
Authorization: Bearer token
```
req.params:
    liked: boolean
    disliked: boolean
    watched_like: boolean
    watched_dislike: boolean
    not_interested: boolean

response:
    status: 200, 404, 500...
    friends: [seznam stringov prijatljov ki se strinjajo]
```

## /user/movies/list
Tip metode: GET
Authorization: Bearer token
```
req.query:
    liked: boolean
    disliked: boolean
    watched_like: boolean
    watched_dislike: boolean
    not_interested: boolean

response:
    status: 200, 404, 500...
    liked: [množica filmov],
    disliked: [množica filmov],
    ...
```

## /user/friends/list
Tip metode: GET
Authorization: Bearer token
```
response:
    status: 200, 404, 500...
    friends: [množica prijatljov]

prijatelji:
{
    "username": string
    "id": id
}
```

## /user/friends/add
Tip metode: POST
Authorization: Bearer token
```
req.params:
    username: string

response:
    status: 200, 404, 500...
    friend_len: integer (št. prijatljev)
```


